package yathzee.app.graphique;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class ChoixNomsController implements Initializable {

	/**
	 * Variables des id des feuilles FXML iJchoixJoueurs (i entre 1 et 4)
	 * Permet de gérer la saisie et la sauvegarde des pseudo des joueurs
	 */
	@FXML
	private AnchorPane root;
	@FXML
	private HBox nomJoueursContainer;
	@FXML
	private VBox saisieNom1;
	@FXML
	private VBox saisieNom2;
	@FXML
	private VBox saisieNom3;
	@FXML
	private VBox saisieNom4;
	@FXML
	private Label nomJ1;
	@FXML
	private Label nomJ2;
	@FXML
	private Label nomJ3;
	@FXML
	private Label nomJ4;
	@FXML
	private TextField fieldJ1;
	@FXML
	private TextField fieldJ2;
	@FXML
	private TextField fieldJ3;
	@FXML
	private TextField fieldJ4;

	private static String fichierNom = "src/main/resources/app/graphique/data/nomJoueurs.txt";


	/**
	 * Fonction qui change de AnchorPane en fonction du nombre de joueurs voulu
	 * @param nbr
	 * @throws IOException
	 */
	@FXML
	protected void ajouterJoueur(int nbr) throws IOException {
		AnchorPane home = null;
		switch (nbr) {
			case 1:
				home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/1JchoixNoms.fxml"));
				break;
			case 2:
				home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/2JchoixNoms.fxml"));
				break;
			case 3:
				home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/3JchoixNoms.fxml"));
				break;
			case 4:
				home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/4JchoixNoms.fxml"));
			default:
				break;
		}
		root.getChildren().setAll(home);

	}


	/**
	 * Fonctions qui gère l'ajout et la suppression des joueurs 
	 * @throws IOException
	 */
	@FXML
	protected void passerDeuxJoueurs() throws IOException {
		ajouterJoueur(2);
	}

	@FXML
	protected void passerTroisJoueurs() throws IOException {
		ajouterJoueur(3);
	}

	@FXML
	protected void passerQuatreJoueurs() throws IOException {
		ajouterJoueur(4);
	}

	@FXML
	protected void descendreUnJoueur() throws IOException {
		ajouterJoueur(1);
	}

	@FXML
	protected void descendreDeuxJoueurs() throws IOException {
		ajouterJoueur(2);
	}

	@FXML
	protected void descendreTroisJoueurs() throws IOException {
		ajouterJoueur(3);
	}

	

	/**
	 * Fonctions qui lance la partie pour un joueur
	 * @throws IOException
	 */
	@FXML
	protected void lancerPlateau1J() throws IOException {
		gestionFichier.ecrireFichier(fichierNom,fieldJ1.getText());
		gestionFichier.initFichierScore(fieldJ1.getText());
		AnchorPane home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/1JPlateau.fxml"));
		root.getChildren().setAll(home);
	}

	/**
	 * Fonctions qui lance la partie pour deux joueurs
	 * @throws IOException
	 */
	@FXML
	protected void lancerPlateau2J() throws IOException {
		gestionFichier.ecrireFichier(fichierNom,fieldJ1.getText());
		gestionFichier.initFichierScore(fieldJ1.getText());
		gestionFichier.ecrireFichier(fichierNom,fieldJ2.getText());
		gestionFichier.initFichierScore(fieldJ2.getText());
		AnchorPane home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/2JPlateau.fxml"));
		root.getChildren().setAll(home);
	}

	/**
	 * Fonctions qui lance la partie pour trois joueurs
	 * @throws IOException
	 */
	@FXML
	protected void lancerPlateau3J() throws IOException {
		gestionFichier.ecrireFichier(fichierNom,fieldJ1.getText());
		gestionFichier.initFichierScore(fieldJ1.getText());
		gestionFichier.ecrireFichier(fichierNom,fieldJ2.getText());
		gestionFichier.initFichierScore(fieldJ2.getText());
		gestionFichier.ecrireFichier(fichierNom,fieldJ3.getText());
		gestionFichier.initFichierScore(fieldJ3.getText());
		AnchorPane home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/3JPlateau.fxml"));
		root.getChildren().setAll(home);
	}

	/**
	 * Fonctions qui lance la partie pour quatre joueurs
	 * @throws IOException
	 */
	@FXML
	protected void lancerPlateau4J() throws IOException {
		gestionFichier.ecrireFichier(fichierNom,fieldJ1.getText());
		gestionFichier.initFichierScore(fieldJ1.getText());
		gestionFichier.ecrireFichier(fichierNom,fieldJ2.getText());
		gestionFichier.initFichierScore(fieldJ2.getText());
		gestionFichier.ecrireFichier(fichierNom,fieldJ3.getText());
		gestionFichier.initFichierScore(fieldJ3.getText());
		gestionFichier.ecrireFichier(fichierNom,fieldJ4.getText());
		gestionFichier.initFichierScore(fieldJ4.getText());
		AnchorPane home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/4JPlateau.fxml"));
		root.getChildren().setAll(home);
	}

	/**
	 * Gestion du bouton précédent des iJchoixJoueurs.fxml
	 * @param e
	 * @throws IOException
	 */
	@FXML
	protected void retourChoixJoueurs(ActionEvent e) throws IOException {
		AnchorPane home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/choixJoueurs.fxml"));
		root.getChildren().setAll(home);
	}


	@Override
	public void initialize(URL location, ResourceBundle resources) {}
		
}
