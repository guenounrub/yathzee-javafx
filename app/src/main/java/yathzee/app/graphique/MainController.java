package yathzee.app.graphique;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;


public class MainController implements Initializable{
	
	@FXML
	private AnchorPane root;

	@FXML
	protected void afficherRegles(ActionEvent e) throws IOException {
		AnchorPane regles = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/regles.fxml"));
		root.getChildren().setAll(regles);
	}
	
	@FXML
	protected void commencerPartie(ActionEvent e) throws IOException {
		AnchorPane nbrJoueurs = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/choixJoueurs.fxml"));
		root.getChildren().setAll(nbrJoueurs);
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {}
	 

}
