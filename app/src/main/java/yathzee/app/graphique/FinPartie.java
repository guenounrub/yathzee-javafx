package yathzee.app.graphique;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class FinPartie implements Initializable {

	@FXML
	AnchorPane root;
	@FXML
	Label labelResultatJ1, labelResultatJ2, labelResultatJ3, labelResultatJ4;

	@FXML
	protected void retourMenu() throws IOException {
		AnchorPane home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/main.fxml"));
		root.getChildren().setAll(home);
	}

	@FXML
	protected void quitterJeu() {
	}

	@FXML
	protected void recommencerPartie() throws IOException {
		int nbJoueurs = gestionFichier.getNombreJoueurs();
		AnchorPane home = null;
		gestionFichier.effacerDataJoueurs();
		switch(nbJoueurs){
			case 1: 
				home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/1JPlateau.fxml"));
				root.getChildren().setAll(home);
				break;
			case 2:// changer redirection une fois mode multijoueur ok
				home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/2JchoixNoms.fxml"));
				root.getChildren().setAll(home);
				break;
			case 3:
				home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/3JchoixNoms.fxml"));
				root.getChildren().setAll(home);
				break;
			case 4:
				home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/4JchoixNoms.fxml"));
				root.getChildren().setAll(home);
				break;
		}
	}

	protected void setLabelsJoueur(int numJoueur){
		String nom = gestionFichier.getNomJoueur(numJoueur);
		switch (numJoueur) {
			case 1:
				labelResultatJ1.setText(nom+" : "+gestionFichier.recalculerScore(nom)+" pts");
				break;
			case 2:
				labelResultatJ2.setText(nom+" : "+gestionFichier.recalculerScore(nom)+" pts");
				break;
			case 3:
				labelResultatJ3.setText(nom+" : "+gestionFichier.recalculerScore(nom)+" pts");
				break;
			case 4:
				labelResultatJ4.setText(nom+" : "+gestionFichier.recalculerScore(nom)+" pts");
				break;
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		int nbJoueurs = gestionFichier.getNombreJoueurs();
		for (int i = 1; i <= nbJoueurs; i++) {
			setLabelsJoueur(i);
		}
	}

}