/**
 * Controlleur du fichier 1JPlateau.fxml 
 * Emplacement du fichier : src/main/resources/graphique/fxml/1JPlateau.fxml
 * 
 * @author Ruben Guenoun <guenoun.ruben@gmail.com>
 * @date 20/04/2020
 * 
 */

package yathzee.app.graphique;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import yathzee.app.console.De;
import yathzee.app.console.Joueur;
import yathzee.app.console.Partie;

public class J2PlateauController implements Initializable {


	/* Variables de la feuille FXML associée : 1JPlateau.fxml */
	@FXML
	AnchorPane root;
	@FXML
	Label nomJoueur1,nomJoueur2;
	@FXML
	Label scoreJ1,scoreJ2, labelBonus, labelAs, labelDeux, labelTrois, labelQuatre, labelCinq, labelSix, labelBrelan, labelCarre, labelFull, labelPS, labelGS, labelYathzee, labelChance, nbrLancerLabel;
	@FXML
	VBox dicesContainer, scoreContainer;
	@FXML
	HBox relance;
	@FXML
	ImageView deR1, deR2, deR3, deR4, deR5, deG1, deG2, deG3, deG4, deG5;
	@FXML
	Button buttonLancer;
	@FXML
	Button buttonAs, buttonDeux, buttonTrois, buttonQuatre, buttonCinq, buttonSix, buttonBrelan, buttonCarre, buttonFull, buttonPS, buttonGS, buttonYathzee, buttonChance;

	static int tour = 1; //gère les tours et les joueurs
	static De[] combi;
	static int nbrLancers = 3;


	/* Fonctions pour la gestion des dés */


	/**
	 * Fct qui change le label du nombre de lancers
	 */
	private void setNbrLancerLabel(){
		nbrLancerLabel.setText(nbrLancers + " lancers restant");
	}

	/**
	 * Fct qui détermine le nombre de Dés à relancer
	 * @return un entier qui indique le nombre de dés à relancer
	 */
	protected int nbrDesRelance() {
		int nbr = 0;
		if (deR1.getImage() != null) {nbr++;}
		if (deR2.getImage() != null) {nbr++;}
		if (deR3.getImage() != null) {nbr++;}
		if (deR4.getImage() != null) {nbr++;}
		if (deR5.getImage() != null) {nbr++;}
		return nbr;
	}

		/**
	 * Procédure de surcharge qui affecte l'image correspondant au numéro du dé passé en paramètre
	 * @param d Dé dont on veut l'image
	 * @param numDe Position du dés sur le plateau
	 */
	private void setImage(De d,int numDe){
		switch (numDe) {
			case 1:
				setImage(d, deR1);
				break;
			case 2:
				setImage(d, deR2);
				break;
			case 3:
				setImage(d, deR3);
				break;
			case 4:
				setImage(d, deR4);
				break;
			case 5:
				setImage(d, deR5);
				break;
		}
	}

	/**
	 * Procédure qui affecte l'image correspondant au dé passé en paramètre
	 * @param d Dé dont on veut l'image 
	 * @param im Emplacement FXML de l'image
	 */
	private void setImage(De d,ImageView im){
		switch(d.getValeur()){
			case 1:
				im.setImage(new Image("app/graphique/imgs/deFaceUnRouge.png"));
				break;
			case 2:
				im.setImage(new Image("app/graphique/imgs/deFaceDeuxRouge.png"));
				break;
			case 3:
				im.setImage(new Image("app/graphique/imgs/deFaceTroisRouge.png"));
				break;
			case 4:
				im.setImage(new Image("app/graphique/imgs/deFaceQuatreRouge.png"));
				break;
			case 5:
				im.setImage(new Image("app/graphique/imgs/deFaceCinqRouge.png"));
				break;
			case 6:
				im.setImage(new Image("app/graphique/imgs/deFaceSixRouge.png"));
				break;
		}
	}

	
	/**
	 * Procédure de relance de dés qui détermine la position des dés à relancer 
	 * et qui relance les dés en affichant les nouveaux dés tirés
	 */
	@FXML
	protected void relancerDes() {
		if (nbrLancers > 0) {
			String nom = gestionFichier.getNomJoueur(tour);
			int[] numDes = new int[nbrDesRelance()];
			int i = 0;
			// détecte les dés à relancer
			if (deR1.getImage() != null) {
				numDes[i] = 1;
				i++;
			}if (deR2.getImage() != null) {
				numDes[i] = 2;
				i++;
			}if (deR3.getImage() != null) {
				numDes[i] = 3;
				i++;
			}if (deR4.getImage() != null) {
				numDes[i] = 4;
				i++;
			}if (deR5.getImage() != null) {
				numDes[i] = 5;
				i++;
			}
			// affiche les dés à relancer
			combi = Partie.relancerDes(combi, numDes);
			for (int j = 0; j < numDes.length; j++) {
				setImage(combi[numDes[j] - 1], numDes[j]);
			}
			getScore(nom);
			nbrLancers--;
			activerBouttons();
			setNbrLancerLabel();
		}
	}

	/**
	 * Fct qui déplace une image de a vers b 
	 * L'image a est alors rempli avec null
	 * @param a Lieu d'origine de l'image 
	 * @param b Lieu d'arrivée de l'image 
	 */
	protected void switchImage(ImageView a, ImageView b) {
		String img = a.getImage().getUrl();
		a.setImage(null);
		b.setImage(new Image(img));
	}

	/**
	 * Procédure de gestion des changement de dés
	 * En fonction du parent on échange avec le bon enfant
	 * @param parent
	 * 
	 * Remarque : Cette fonction sert de passerelle à toutes les fonctions des Images "onClick" du FXML
	 */
	protected void selDe(ImageView parent) {
		if (parent.equals(deR1)) {
			switchImage(deR1, deG1);
		}if (parent.equals(deR2)) {
			switchImage(deR2, deG2);
		}if (parent.equals(deR3)) {
			switchImage(deR3, deG3);
		}if (parent.equals(deR4)) {
			switchImage(deR4, deG4);
		}if (parent.equals(deR5)) {
			switchImage(deR5, deG5);
		}if (parent.equals(deG1)) {
			switchImage(deG1, deR1);
		}if (parent.equals(deG2)) {
			switchImage(deG2, deR2);
		}if (parent.equals(deG3)) {
			switchImage(deG3, deR3);
		}if (parent.equals(deG4)) {
			switchImage(deG4, deR4);
		}if (parent.equals(deG5)) {
			switchImage(deG5, deR5);
		}}
	
	// Fonctions FXML des image lorsqu'on clique dessus 
	@FXML
	protected void selectDe1(){selDe(deR1);}
	@FXML
	protected void selectDe2(){selDe(deR2);}
	@FXML
	protected void selectDe3(){selDe(deR3);}
	@FXML
	protected void selectDe4(){selDe(deR4);}
	@FXML
	protected void selectDe5(){selDe(deR5);}
	@FXML
	protected void unselectDe1(){selDe(deG1);}
	@FXML
	protected void unselectDe2(){selDe(deG2);}
	@FXML
	protected void unselectDe3(){selDe(deG3);}
	@FXML
	protected void unselectDe4(){selDe(deG4);}
	@FXML
	protected void unselectDe5(){selDe(deG5);}

	
	/**
	 * Procédure de remise à zéro du plateau de jeu 
	 */
	private void resetPlateau() {
		deR1.setImage(new Image("app/graphique/imgs/deFaceNullRouge.png"));
		deR2.setImage(new Image("app/graphique/imgs/deFaceNullRouge.png"));
		deR3.setImage(new Image("app/graphique/imgs/deFaceNullRouge.png"));
		deR4.setImage(new Image("app/graphique/imgs/deFaceNullRouge.png"));
		deR5.setImage(new Image("app/graphique/imgs/deFaceNullRouge.png"));
		deG1.setImage(null);
		deG2.setImage(null);
		deG3.setImage(null);
		deG4.setImage(null);
		deG5.setImage(null);
		desactiverBouttons();
		updateJoueur();
		switchJoueur();
		resetScore(gestionFichier.getNomJoueur(tour));
	}



	/* Fonctions de gestions des boutons du //score */


	/**
	 * Procédure de redirection une fois la partie terminée
	 * @throws IOException
	 */
	protected void finirPartie() throws IOException {
		if(gestionPartie.isPartieTerminee(gestionFichier.getNomJoueur(gestionFichier.getNombreJoueurs()))){
			AnchorPane fin = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/finPartie.fxml"));
			root.getChildren().setAll(fin);
		}
	}

	/**
	 * Procédure modifiant le label en cas de bonus sur la catégorie supérieure
	 */
	private void setLabelBonus() {
		String[] valeursScores = gestionFichier.getValeursScore(gestionFichier.getNomJoueur(tour));
		int scorePartieSup = 0;
		for (int i = 0; i < 6; i++) {
			if (valeursScores[i].equals(gestionFichier.getDefaultchar())){
				scorePartieSup += 0;
			}else{
				scorePartieSup += Integer.parseInt(valeursScores[i]);		
			}
		}
		if (scorePartieSup >= 63) {
			labelBonus.setText("35");			
		}else{
			labelBonus.setText("0");
		}
	}


	/** 
	 * Fonctions qui calcul le //score des combinaisons du jeu
	 * 
	 * Comme il est impossible de passer des paramètres aux fonctions des boutons en FXML 
	 * il y a de la duplication de code
	 * 
	 * Description des fonctions : 
	 * 		1. On récupère les //score du joueur
	 * 		2. On vérifie si le //score a déja été marqué
	 * 			a. Si oui, alors on termine et on ne fait rien
	 * 			b. Si non, alors on calcul le nouveau //score, on le sauvegarde dans le fichier de //score du joueur,
	 * 				on calcule le nouveau //score final du joueur,
	 * 				on rétablie ensuite le nbr de lancers pour le tour suivant.
	 * 		  
	 */


	/**
	 * Fct qui permet de désaciver les bouttons tant que le joueur n'a pas lancer les dés
	 * @param isDisable True pour descativer les bouttons, false pour les activer
	 */
	protected void gererBouttons(boolean isDisable){
		buttonAs.setDisable(isDisable);
		buttonDeux.setDisable(isDisable);
		buttonTrois.setDisable(isDisable);
		buttonQuatre.setDisable(isDisable); 
		buttonCinq.setDisable(isDisable); 
		buttonSix.setDisable(isDisable); 
		buttonBrelan.setDisable(isDisable); 
		buttonCarre.setDisable(isDisable);
		buttonFull.setDisable(isDisable);
		buttonPS.setDisable(isDisable);
		buttonGS.setDisable(isDisable);
		buttonYathzee.setDisable(isDisable);
		buttonChance.setDisable(isDisable);
	}

	protected void activerBouttons(){gererBouttons(false);}
	protected void desactiverBouttons(){gererBouttons(true);}
	
	@FXML
	protected void setAs() throws IOException {
		String[] valeursScores = gestionFichier.getValeursScore(gestionFichier.getNomJoueur(tour));
		if (valeursScores[0].equals(gestionFichier.getDefaultchar())){
			int scoreCombi = Joueur.renvoyerScore("As",combi);
			valeursScores[0] = Integer.toString(scoreCombi);
			gestionFichier.sauverScoreJoueur(gestionFichier.getNomJoueur(tour),valeursScores);
			resetScore(gestionFichier.getNomJoueur(tour));
			setScoreTotal();
			resetPlateau();
			nbrLancers = 3;
			setNbrLancerLabel();
			finirPartie();
		}
	}


	@FXML
	protected void setDeux() throws IOException {
		String[] valeursScores = gestionFichier.getValeursScore(gestionFichier.getNomJoueur(tour));
		if (valeursScores[1].equals(gestionFichier.getDefaultchar())){
			int scoreCombi = Joueur.renvoyerScore("Deux",combi);
			valeursScores[1] = Integer.toString(scoreCombi);
			gestionFichier.sauverScoreJoueur(gestionFichier.getNomJoueur(tour),valeursScores);
			resetScore(gestionFichier.getNomJoueur(tour));
			setScoreTotal();
			resetPlateau();
			nbrLancers = 3;
			setNbrLancerLabel();
			finirPartie();
		}
	}
	@FXML
	protected void setTrois() throws IOException {
		String[] valeursScores = gestionFichier.getValeursScore(gestionFichier.getNomJoueur(tour));
		if (valeursScores[2].equals(gestionFichier.getDefaultchar())){
			int scoreCombi = Joueur.renvoyerScore("Trois",combi);
			valeursScores[2] = Integer.toString(scoreCombi);
			gestionFichier.sauverScoreJoueur(gestionFichier.getNomJoueur(tour),valeursScores);
			resetScore(gestionFichier.getNomJoueur(tour));
			setScoreTotal();
			resetPlateau();
			nbrLancers = 3;
			setNbrLancerLabel();
			finirPartie();
		}
	}
	@FXML
	protected void setQuatre() throws IOException {
		String[] valeursScores = gestionFichier.getValeursScore(gestionFichier.getNomJoueur(tour));
		if (valeursScores[3].equals(gestionFichier.getDefaultchar())){
			int scoreCombi = Joueur.renvoyerScore("Quatre",combi);
			valeursScores[3] = Integer.toString(scoreCombi);
			gestionFichier.sauverScoreJoueur(gestionFichier.getNomJoueur(tour),valeursScores);
			resetScore(gestionFichier.getNomJoueur(tour));
			setScoreTotal();
			resetPlateau();
			nbrLancers = 3;
			setNbrLancerLabel();
			finirPartie();
		}
	}
	@FXML
	protected void setCinq() throws IOException {
		String[] valeursScores = gestionFichier.getValeursScore(gestionFichier.getNomJoueur(tour));
		if (valeursScores[4].equals(gestionFichier.getDefaultchar())){
			int scoreCombi = Joueur.renvoyerScore("Cinq",combi);
			valeursScores[4] = Integer.toString(scoreCombi);
			gestionFichier.sauverScoreJoueur(gestionFichier.getNomJoueur(tour),valeursScores);
			resetScore(gestionFichier.getNomJoueur(tour));
			setScoreTotal();
			resetPlateau();
			nbrLancers = 3;
			setNbrLancerLabel();
			finirPartie();
		}
	}
	@FXML
	protected void setSix() throws IOException {
		String[] valeursScores = gestionFichier.getValeursScore(gestionFichier.getNomJoueur(tour));
		if (valeursScores[5].equals(gestionFichier.getDefaultchar())){
			int scoreCombi = Joueur.renvoyerScore("Six",combi);
			valeursScores[5] = Integer.toString(scoreCombi);
			gestionFichier.sauverScoreJoueur(gestionFichier.getNomJoueur(tour),valeursScores);
			resetScore(gestionFichier.getNomJoueur(tour));
			setScoreTotal();
			resetPlateau();
			nbrLancers = 3;
			setNbrLancerLabel();
			finirPartie();
		}
	}
	

	@FXML
	protected void setBrelan() throws IOException {
		String[] valeursScores = gestionFichier.getValeursScore(gestionFichier.getNomJoueur(tour));
		if (valeursScores[6].equals(gestionFichier.getDefaultchar())){
			int scoreCombi = Joueur.renvoyerScore("Brelan",combi);
			valeursScores[6] = Integer.toString(scoreCombi);
			gestionFichier.sauverScoreJoueur(gestionFichier.getNomJoueur(tour),valeursScores);
			resetScore(gestionFichier.getNomJoueur(tour));
			setScoreTotal();
			resetPlateau();
			nbrLancers = 3;
			setNbrLancerLabel();
			finirPartie();
		}
	}

	@FXML
	protected void setCarre() throws IOException {
		String[] valeursScores = gestionFichier.getValeursScore(gestionFichier.getNomJoueur(tour));
		if (valeursScores[7].equals(gestionFichier.getDefaultchar())){
			int scoreCombi = Joueur.renvoyerScore("Carré",combi);
			valeursScores[7] = Integer.toString(scoreCombi);
			gestionFichier.sauverScoreJoueur(gestionFichier.getNomJoueur(tour),valeursScores);
			resetScore(gestionFichier.getNomJoueur(tour));
			setScoreTotal();
			resetPlateau();
			nbrLancers = 3;
			setNbrLancerLabel();
			finirPartie();
		}
	}

	@FXML
	protected void setFull() throws IOException {
		String[] valeursScores = gestionFichier.getValeursScore(gestionFichier.getNomJoueur(tour));
		if (valeursScores[8].equals(gestionFichier.getDefaultchar())){
			int scoreCombi = Joueur.renvoyerScore("Full",combi);
			valeursScores[8] = Integer.toString(scoreCombi);
			gestionFichier.sauverScoreJoueur(gestionFichier.getNomJoueur(tour),valeursScores);
			resetScore(gestionFichier.getNomJoueur(tour));
			setScoreTotal();
			resetPlateau();
			nbrLancers = 3;
			setNbrLancerLabel();
			finirPartie();
		}
	}

	@FXML
	protected void setPS() throws IOException {
		String[] valeursScores = gestionFichier.getValeursScore(gestionFichier.getNomJoueur(tour));
		if (valeursScores[9].equals(gestionFichier.getDefaultchar())){
			int scoreCombi = Joueur.renvoyerScore("Petite suite",combi);
			valeursScores[9] = Integer.toString(scoreCombi);
			gestionFichier.sauverScoreJoueur(gestionFichier.getNomJoueur(tour),valeursScores);
			resetScore(gestionFichier.getNomJoueur(tour));
			setScoreTotal();
			resetPlateau();
			nbrLancers = 3;
			setNbrLancerLabel();
			finirPartie();
		}
	}

	@FXML
	protected void setGS() throws IOException {
		String[] valeursScores = gestionFichier.getValeursScore(gestionFichier.getNomJoueur(tour));
		if (valeursScores[10].equals(gestionFichier.getDefaultchar())){
			int scoreCombi = Joueur.renvoyerScore("Grande suite",combi);
			valeursScores[10] = Integer.toString(scoreCombi);
			gestionFichier.sauverScoreJoueur(gestionFichier.getNomJoueur(tour),valeursScores);
			resetScore(gestionFichier.getNomJoueur(tour));
			setScoreTotal();
			resetPlateau();
			nbrLancers = 3;
			setNbrLancerLabel();
			finirPartie();
		}
	}

	@FXML
	protected void setYathzee() throws IOException {
		String[] valeursScores = gestionFichier.getValeursScore(gestionFichier.getNomJoueur(tour));
		if (valeursScores[11].equals(gestionFichier.getDefaultchar())){
			int scoreCombi = Joueur.renvoyerScore("Yathzee",combi);
			valeursScores[11] = Integer.toString(scoreCombi);
			gestionFichier.sauverScoreJoueur(gestionFichier.getNomJoueur(tour),valeursScores);
			resetScore(gestionFichier.getNomJoueur(tour));
			setScoreTotal();
			resetPlateau();
			nbrLancers = 3;
			setNbrLancerLabel();
			finirPartie();
		}
	}

	@FXML
	protected void setChance() throws IOException {
		String[] valeursScores = gestionFichier.getValeursScore(gestionFichier.getNomJoueur(tour));
		if (valeursScores[12].equals(gestionFichier.getDefaultchar())){
			int scoreCombi = Joueur.renvoyerScore("Chance",combi);
			valeursScores[12] = Integer.toString(scoreCombi);
			gestionFichier.sauverScoreJoueur(gestionFichier.getNomJoueur(tour),valeursScores);
			resetScore(gestionFichier.getNomJoueur(tour));
			setScoreTotal();
			resetPlateau();
			nbrLancers = 3;
			setNbrLancerLabel();
			finirPartie();
		}
	}
	

	/**
	 * Fct qui ecrit les scores du joueurs avec en plus des scores générés par la combinaison des dés 
	 * @param nom Nom du joueur
	 */
	private void getScore(String nom){
		File f = new File(gestionFichier.getPathToData()+nom+".txt");
		String[] valeursScores = gestionFichier.lireFichier(f);
		labelAs.setText((valeursScores[0].equals(gestionFichier.getDefaultchar())) ? valeursScores[0] + " ("+Joueur.renvoyerScore("As",combi)+")":valeursScores[0]);
		labelDeux.setText((valeursScores[1].equals(gestionFichier.getDefaultchar())) ? valeursScores[1] + " ("+Joueur.renvoyerScore("Deux",combi)+")":valeursScores[1]);
		labelTrois.setText((valeursScores[2].equals(gestionFichier.getDefaultchar())) ? valeursScores[2] + " ("+Joueur.renvoyerScore("Trois",combi)+")":valeursScores[2]);
		labelQuatre.setText((valeursScores[3].equals(gestionFichier.getDefaultchar())) ? valeursScores[3] + " ("+Joueur.renvoyerScore("Quatre",combi)+")":valeursScores[3]);
		labelCinq.setText((valeursScores[4].equals(gestionFichier.getDefaultchar())) ? valeursScores[4] + " ("+Joueur.renvoyerScore("Cinq",combi)+")":valeursScores[4]);
		labelSix.setText((valeursScores[5].equals(gestionFichier.getDefaultchar())) ? valeursScores[5] + " ("+Joueur.renvoyerScore("Six",combi)+")":valeursScores[5]);
		labelBrelan.setText((valeursScores[6].equals(gestionFichier.getDefaultchar())) ? valeursScores[6] + " ("+Joueur.renvoyerScore("Brelan",combi)+")":valeursScores[6]);
		labelCarre.setText((valeursScores[7].equals(gestionFichier.getDefaultchar())) ? valeursScores[7] + " ("+Joueur.renvoyerScore("Carré",combi)+")":valeursScores[7]);
		labelFull.setText((valeursScores[8].equals(gestionFichier.getDefaultchar())) ? valeursScores[8] + " ("+Joueur.renvoyerScore("Full",combi)+")":valeursScores[8]);
		labelPS.setText((valeursScores[9].equals(gestionFichier.getDefaultchar())) ? valeursScores[9] + " ("+Joueur.renvoyerScore("Petite suite",combi)+")":valeursScores[9]);
		labelGS.setText((valeursScores[10].equals(gestionFichier.getDefaultchar())) ? valeursScores[10] + " ("+Joueur.renvoyerScore("Grande suite",combi)+")":valeursScores[10]);
		labelYathzee.setText((valeursScores[11].equals(gestionFichier.getDefaultchar())) ? valeursScores[11] + " ("+Joueur.renvoyerScore("Yathzee",combi)+")":valeursScores[11]);
		labelChance.setText((valeursScores[12].equals(gestionFichier.getDefaultchar())) ? valeursScores[12] + " ("+Joueur.renvoyerScore("Chance",combi)+")":valeursScores[12]);
		setLabelBonus();
	}

	/**
	 * Fct qui affiche seulement les scores du joueur pdt l'entre deux tours
	 * @param nom Nom du joueur
	 */
	private void resetScore(String nom){
		File f = new File(gestionFichier.getPathToData()+nom+".txt");
		String[] valeursScores = gestionFichier.lireFichier(f);
		labelAs.setText(valeursScores[0]);
		labelDeux.setText(valeursScores[1]);
		labelTrois.setText(valeursScores[2]);
		labelQuatre.setText(valeursScores[3]);
		labelCinq.setText(valeursScores[4]);
		labelSix.setText(valeursScores[5]);
		labelBrelan.setText(valeursScores[6]);
		labelCarre.setText(valeursScores[7]);
		labelFull.setText(valeursScores[8]);
		labelPS.setText(valeursScores[9]);
		labelGS.setText(valeursScores[10]);
		labelYathzee.setText(valeursScores[11]);
		labelChance.setText(valeursScores[12]);
		setLabelBonus();
	}


	/* Fonctions permettants de changer de joueurs */
	private void switchJoueur(){
		switch(tour){
			case 1:
				nomJoueur1.setId("joueur");
				scoreJ1.setId("joueur");
				nomJoueur2.setId("spectateur");
				scoreJ2.setId("spectateur");
				break;
			case 2:
				nomJoueur1.setId("spectateur");
				scoreJ1.setId("spectateur");
				nomJoueur2.setId("joueur");
				scoreJ2.setId("joueur");
				break;
		}
	}

	/* Fct permettant de passer au tour suivant */
	private void updateJoueur(){
		if(tour < gestionFichier.getNombreJoueurs()){
			tour++;
		}else{
			tour = 1;
		}
	}

	/* Actualise le score du joueur en cours */
	private void setScoreTotal(){
		int score = gestionFichier.recalculerScore(gestionFichier.getNomJoueur(tour));
		switch(tour){
			case 1:
				scoreJ1.setText(Integer.toString(score)+" pts");
				break;
			case 2:
				scoreJ2.setText(Integer.toString(score)+" pts");
				break;
		}
	}

	/* Initialise les noms des joueurs */
	private void setNoms(){
		String[] noms = gestionFichier.lireFichier(new File(gestionFichier.getPathToNomJoueurs()));
		nomJoueur1.setText(noms[0]);
		nomJoueur2.setText(noms[1]);
	}



	/**
	 * Procédure d'initialisation de la fenêtre
	 * Initialise le nom du joueur, et le nbr de lancers
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		switchJoueur();
		desactiverBouttons();
		setNoms();
		setNbrLancerLabel();
	}
}