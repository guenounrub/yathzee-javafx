package yathzee.app.graphique;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

//import console.*;

public class gestionFichier {

	/**
	 * Variables Statiques de Chemins vers fichiers et données
	 */
	private static String pathToData = "src/main/resources/app/graphique/data/";
	private static String defaultChar = "-";

	public static String getPathToData(){
		return pathToData;
	}

	public static String getPathToNomJoueurs() {
		return pathToData+"nomJoueurs.txt";
	}

	public static String getDefaultchar() {
		return defaultChar;
	}


	/* Fonctions de base de manipulation de fichier */

	/**
	 * Fonction qui retourne le nbr de ligne d'un fichier
	 * @param fichier Fichier dont on veut connaitre le nbr de ligne
	 * @return Le nombre de ligne dans le fichier
	 */
	public static int nbrLignes(File fichier) {
		int nbr = 0;
		Scanner sc;
		try {
			sc = new Scanner(fichier);
			while (sc.hasNextLine()) {
				sc.nextLine();
				nbr++;
			}
			sc.close();
		} catch (Exception e) {
			System.out.println("Erreur fichier dans nbrLignes , fichier "+fichier.getName());
		}
		return nbr;
	}

	/**
	 * Fonction qui affiche un tableau de String
	 * Sert à afficher le contenue d'un fichier
	 * @param t Tableau à afficher
	 */
	public static void afficherTab(String[] t) {
		System.out.println("Tableau de String : ");
		for (int i = 0; i < t.length; i++) {
			System.out.println(t[i]);
		}
		System.out.println("Fin affichage");

	}

	/**
	 * Fonction de lecture de fichier
	 * @param fichier Fichier dont on veut récupérer le contenu
	 * @return Le contenue du fichier sous forme de tableau de String
	 */
	public static String[] lireFichier(File fichier) {
		Scanner sc;
		String[] lignes = new String[nbrLignes(fichier)];
		int i = 0;
		try {
			sc = new Scanner(fichier);
			while (sc.hasNextLine()) {
				lignes[i] = sc.nextLine();
				i++;
			}
			sc.close();
		} catch (Exception e) {
			System.out.println("Erreur fichier dans lireFichier , fichier : "+fichier.getName());
		}
		return lignes;
	}

	/**
	 * Fct qui crée un fichier vide à l'emplacement passé en paramètre
	 * @param pathToFile Chemin du fichier
	 * @throws IOException
	 */
	public static void creerFichierVide(String pathToFile) throws IOException {
		File newfichier = new File(pathToFile);
		newfichier.createNewFile();
	}

	/**
	 * Fct qui efface le contenu d'un fichier
	 * @param pathToFile Chemin du fichier
	 * @throws IOException
	 */
	public static void effacerContenueFichier(String pathToFile) throws IOException {
		File fichier = new File(pathToFile);
		fichier.delete();
		creerFichierVide(pathToFile);
	}

	/**
	 * Fonction qui écrit chaine dans le fichier se trouvant dans pathToFile
	 * @param pathToFile Chemin du fichier
	 * @param chaine chaine à écrire
	 * @param retourLigne ajoute un renvoie à ligne si true
	 * @throws IOException
	 */
	public static void ecrireFichier(String pathToFile,String chaine, boolean retourLigne) throws IOException {
		if(retourLigne){
			chaine = chaine+"\n";
		}
		FileWriter writer = null;
		try {
			// Ouverture du fichier en écriture
			writer = new FileWriter(pathToFile, true);
			writer.append(chaine);//, 0, nom.length());
		}
		catch(FileNotFoundException ex){
			System.out.println("Fichier non trouvé dans ecrireFichier");
			System.out.println("Repertoire courant : "+new File(".").getAbsolutePath());
		}
		finally{
		 	if(writer != null){
		 		try {
		 			/* Fermeture du flux */
		 			writer.close();
		 		}
		 		catch (IOException e) {
					System.out.println("erreur de fermeture de fichier"); 
		 		}
		 	}
		}
	}

	/**
	 * Ecrit avec retour à la ligne implicit
	 * @param pathToFile
	 * @param chaine
	 * @throws IOException
	 */
	public static void ecrireFichier(String pathToFile, String chaine) throws IOException {
		ecrireFichier(pathToFile,chaine,true);
	}



	/* Fonctions spécifique à l'application */

	/**
	 * Fct qui initialise le fichier de score du joueur nom
	 * @param nom Nom du joueur
	 * @throws IOException
	 */
	public static void initFichierScore(String nom) throws IOException {
		String file = pathToData+nom+".txt";
		creerFichierVide(file);
		for (int i = 0; i < 13; i++) {
			ecrireFichier(file,defaultChar);			
		}
	}

	/**
	 * Fct qui supprime les fichiers du jeu pour ne pas surcharger le Disque
	 */
	public static void effacerDataJoueurs(){
		String[] prenoms = lireFichier(new File(getPathToNomJoueurs()));
		for (int i = 0; i < prenoms.length; i++) {
			new File(getPathToData()+prenoms[i]+".txt").delete();
		}
	}

	public static void effacerData() throws IOException {
		effacerDataJoueurs();
		effacerContenueFichier(getPathToData()+"nomJoueurs.txt");
	}

	/**
	 * Fct qui Ecrit dans le fichier du joueur nom sa nouvelle table de score
	 * @param nom Nom du joueur
	 * @param valeursScores Tableau de String correspondant aux differents scores
	 * @throws IOException
	 */
	public static void sauverScoreJoueur(String nom, String[] valeursScores) throws IOException {
		String file = pathToData+nom+".txt";
		effacerContenueFichier(file);
		for (int i = 0; i < valeursScores.length; i++) {
			ecrireFichier(file, valeursScores[i]);
		}
	}

	/**
	 * Fct qui retourne le nbr de joueurs en jeu
	 * @return Le nombre de joueurs en jeu
	 */
	public static int getNombreJoueurs() {
		return nbrLignes(new File(getPathToNomJoueurs()));
	}

	public static String getNomJoueur(int numJoueur){
		String[] noms = lireFichier(new File(getPathToNomJoueurs()));
		return noms[numJoueur-1];
	}

	/**
	 * Fct qui récupère les score du joueur dont le nom est donné en paramètre
	 * @param nom Nom du joueur dont on veut les scores
	 * @return Tableau de String avec chaque i correspondant à une combi
	 */
	public static String[] getValeursScore(String nom) {
		File f = new File(getPathToData() + nom + ".txt");
		String[] valeursScores = lireFichier(f);
		return valeursScores;
	}

		/**
	 * Fct qui recalcule le score du joueur dont le nom est donné en paramètre 
	 * @param nom Nom du joueur dont on veut recalculer le score
	 * @return L'entier valant le score
	 */
	public static int recalculerScore(String nom) {	
		int score = 0;
		String[] valeursScores = getValeursScore(nom);
		for (int i = 0; i < valeursScores.length; i++) {
			if (valeursScores[i].equals(getDefaultchar())){
				score += 0;				
			}else{
				score += Integer.parseInt(valeursScores[i]);
			}
		}
		return score;
	}

}