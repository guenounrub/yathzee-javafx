package yathzee.app.graphique;

public class gestionPartie {
	/**
	 * Fct qui détermine si la partie est terminé
	 * On vérifie pour cela si le joueur à rempli toutes les cases de scores
	 * @param nom Nom du joueur dont on veut vérifier les scores
	 * @return True si la partie est terminée, False sinon
	 */
	public static boolean isPartieTerminee(String nom){
		String[] valeursScores = gestionFichier.getValeursScore(nom);
		int nbrScoresNonRemplis = 0;
		for (int i = 0; i < valeursScores.length; i++) {
			if(valeursScores[i].equals(gestionFichier.getDefaultchar())){
				nbrScoresNonRemplis++;
			}
		}
		return nbrScoresNonRemplis == 0;
	}
}