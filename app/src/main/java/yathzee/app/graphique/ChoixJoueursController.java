package yathzee.app.graphique;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

public class ChoixJoueursController implements Initializable{
	
	@FXML
	private AnchorPane nbrJoueurs;
	
	/**
	 * Gestion des boutons de joueurs 
	 * @param e
	 * @throws IOException
	 */
	@FXML
	protected void demarrerUnJoueur(ActionEvent e) throws IOException {
		AnchorPane home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/1JchoixNoms.fxml"));
		nbrJoueurs.getChildren().setAll(home);
	}
	
	@FXML
	protected void demarrerDeuxJoueurs(ActionEvent e) throws IOException {
		AnchorPane home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/2JchoixNoms.fxml"));
		nbrJoueurs.getChildren().setAll(home);
	}
	
	@FXML
	protected void demarrerTroisJoueurs(ActionEvent e) throws IOException{
		AnchorPane home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/3JchoixNoms.fxml"));
		nbrJoueurs.getChildren().setAll(home);
	}
	
	@FXML
	protected void demarrerQuatreJoueurs(ActionEvent e) throws IOException{
		AnchorPane home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/4JchoixNoms.fxml"));
		nbrJoueurs.getChildren().setAll(home);
	}
	
	/**
	 * Retour à l'écran précédent 
	 * @param e
	 * @throws IOException
	 */
	@FXML
	protected void retourMenu(ActionEvent e) throws IOException {
		AnchorPane home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/main.fxml"));
		nbrJoueurs.getChildren().setAll(home);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {}

}
