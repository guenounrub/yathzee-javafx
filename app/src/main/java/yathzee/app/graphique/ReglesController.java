package yathzee.app.graphique;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

public class ReglesController implements Initializable{
	
	@FXML
	private AnchorPane regles;
	
	@FXML
	protected void retourMenu(ActionEvent e) throws IOException {
		AnchorPane home = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/main.fxml"));
		regles.getChildren().setAll(home);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {}

}
