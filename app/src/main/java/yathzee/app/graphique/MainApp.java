package yathzee.app.graphique;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application {

	@Override
	public void start(Stage primaryStage) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("/app/graphique/fxml/main.fxml"));
		Scene scene = new Scene(root,690,503);
		primaryStage.setScene(scene);
		primaryStage.show();
		primaryStage.setOnCloseRequest(e-> {
			try {
				gestionFichier.effacerData();
			} catch (IOException e1) {
				System.out.println("Erreur d'effacement des fichiers");
			}
		});

	}
	
	public static void main(String[] args) {
		launch(args);
	}

}
