/**
 * @author Ruben Guenoun <guenoun.ruben@gmail.com>
 * @date 27/03/2020
 * Tous droits réservés
 * Calcul des scores des combinaisons
 */

package yathzee.app.console;


public class De {
	private int valeur;
	
	public De() {
		this.valeur = 0;
	}
	
	public De(int valeur) {
		this.valeur = valeur;
	}
	
	public int getValeur() {
		return this.valeur;
	}
	
	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	public void lancer() {
		double nbr = Math.random()*6;
		this.valeur = 1 + (int) nbr;
	}
	
	public boolean equals(Object obj) {
		if (obj instanceof De) {
			De d = (De)obj;
			return this.valeur == d.getValeur();
		}else {
			return false;
		}
	}
	
	public static boolean superieur(De d1, De d2) {
		return d1.getValeur() > d2.getValeur();
	}
	
	public static De[] recopieCombi(De[] combi) {
		De[] c = new De[combi.length];
		for (int i =0;i <= combi.length-1;i++) {
			c[i] = new De(combi[i].getValeur());
		}
		return c;
	}
	
	public static void trierCombi(De[] combi) {
		De tmp;
		boolean desordre = true;
		while(desordre) {
			desordre = false;
			for (int i =0;i <= combi.length-2;i++) {
				if(superieur(combi[i],combi[i+1])) {
					desordre = true;
					tmp = new De(combi[i+1].getValeur());
					combi[i+1].setValeur(combi[i].getValeur());
					combi[i].setValeur(tmp.getValeur());
				}
			}
		}
	}
	
	public static int sommeCombi(De[] combi) {
		int somme = 0;
		for (int i =0;i <= combi.length-1;i++) {
			somme += combi[i].getValeur();
		}
		return somme;
	}
	
	public static int scoreChance(De[] combi) {
		return sommeCombi(combi);
	}
	
	public static int scoreHaut(De[] combi,int valeur){
		int compteur = 0;
		for (int j =0;j <= combi.length-1;j++) {
			if(combi[j].getValeur() == valeur) {
				compteur++;
			}
		}
		return compteur*valeur ;
	}


	public static int scoreBrelan(De[] combi) {
		boolean is3DesIdentiques = false;
		int compteur = 0;
		for (int i =1;i <= 6;i++) {
			compteur = 0;
			for (int j =0;j <= combi.length-1;j++) {
				if(combi[j].getValeur() == i) {
					compteur++;
				}
				if(compteur >= 3) {
					is3DesIdentiques = true;
				}
			}
		}
		if(is3DesIdentiques) {
			return sommeCombi(combi);
		}else {
			return 0;
		}
	}
	
	public static int scoreCarre(De[] combi) {
		boolean is4DesIdentiques = false;
		int compteur = 0;
		for (int i =1;i <= 6;i++) {
			compteur = 0;
			for (int j =0;j <= combi.length-1;j++) {
				if(combi[j].getValeur() == i) {
					compteur++;
				}
				if(compteur >= 4) {
					is4DesIdentiques = true;
				}
			}
		}
		if(is4DesIdentiques) {
			return sommeCombi(combi);
		}else {
			return 0;
		}
	}
	
	public static int scoreYathzee(De[] combi) {
		boolean is5DesIdentiques = false;
		int compteur = 0;
		for (int i =1;i <= 6;i++) {
			compteur = 0;
			for (int j =0;j <= combi.length-1;j++) {
				if(combi[j].getValeur() == i) {
					compteur++;
				}
				if(compteur >= 5) {
					is5DesIdentiques = true;
				}
			}
		}
		if(is5DesIdentiques) {
			return 50;
		}else {
			return 0;
		}
	}
	

	public static int scorePteSuite(De[] combi) {
		De[] c = recopieCombi(combi);
		trierCombi(c);
		// on supprime les eventuels doublons
		for (int i =0;i <= c.length-2;i++) {
			if (c[i].equals(c[i+1])) {
				c[i].setValeur(0);
			}
		}
		trierCombi(c);
		if ((c[0].getValeur() == 1 && c[1].getValeur() == 2 && c[2].getValeur() == 3 && c[3].getValeur() == 4) 
			|| (c[0].getValeur() == 2 && c[1].getValeur() == 3 && c[2].getValeur() == 4 && c[3].getValeur() == 5) 
			|| (c[0].getValeur() == 3 && c[1].getValeur() == 4 && c[2].getValeur() == 5 && c[3].getValeur() == 6) 
			|| (c[1].getValeur() == 1 && c[2].getValeur() == 2 && c[3].getValeur() == 3 && c[4].getValeur() == 4) 
			|| (c[1].getValeur() == 2 && c[2].getValeur() == 3 && c[3].getValeur() == 4 && c[4].getValeur() == 5) 
			|| (c[1].getValeur() == 3 && c[2].getValeur() == 4 && c[3].getValeur() == 5 && c[4].getValeur() == 6)) {
			return 30;
		}else {
			return 0;
		}
	}
	
	public static int scoreGdeSuite(De[] combi) {
		De[] c = recopieCombi(combi);
		trierCombi(c);
		if ((c[0].getValeur() == 1 && c[1].getValeur() == 2 && c[2].getValeur() == 3 && c[3].getValeur() == 4 && c[4].getValeur() == 5)
			|| (c[0].getValeur() == 2 && c[1].getValeur() == 3 && c[2].getValeur() == 4 && c[3].getValeur() == 5 && c[4].getValeur() == 6)) {
			return 40;
		}else {
			return 0;				
		}
	}
	
	public static int scoreFull(De[] combi) {
		De[] c = recopieCombi(combi);
		trierCombi(c);
		if(((c[0].getValeur()==c[1].getValeur() && c[1].getValeur()==c[2].getValeur() && c[3].getValeur()==c[4].getValeur()) && c[2].getValeur()!=c[3].getValeur())
		 || ((c[0].getValeur()==c[1].getValeur() && c[1].getValeur()!=c[2].getValeur()) && c[2].getValeur()==c[3].getValeur() && c[3].getValeur()==c[4].getValeur())) {
			return 25;
		}else {
			return 0;
		}
	}
	
	
	
}
