/**
 * @author Ruben Guenoun <guenoun.ruben@gmail.com>
 * @date 27/03/2020
 * Tous droits réservés
 */
package yathzee.app.console;

import java.util.Map;
import java.util.Set;

public class Joueur {
	private String nom;
	private FeuilleScore f;
	private int score;
	
	public Joueur(String nom) {
		this.nom = nom;
		this.score = 0;
		this.f = new FeuilleScore();
		
	}

	public FeuilleScore getF() {
		return f;
	}

	public void setF(FeuilleScore f) {
		this.f = f;
	}

	public String getNom() {
		return nom;
	}

	public int getScore() {
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}

	public void ajouterPoints(int pts) {
		this.score += pts;
	}
	
	public String toString() {
		return "Le joueur "+this.nom+" a un score de "+this.score+" pts.";
	}
	
	/**
	 * Affiche le score du joueur
	 */
	public void afficherScore() {
		Set<String> keys = this.f.getFeuille().keySet();
		System.out.println(this.nom+", voici votre score :");
		for (String key: keys) {
			if (this.f.getFeuille().get(key) == -1) {
				System.out.println(key+" : ");
			}else {
				System.out.println(key+" : "+this.f.getFeuille().get(key)+" pts");
			}
		}
		System.out.println("Total : "+this.score+" pts");
	}
	
	
	
	/**
	 * Calcul le nombre de combinaisons non cochés
	 * @param f le dictionnaire de feuille de score
	 * @return le nombre de scores encore non choisis
	 */
	public static int nbrScoresRestants(Map<String,Integer> f) {
		Set<String> keys = f.keySet();
		int i = 0;
		for (String key: keys) {
			if (f.get(key) == -1) {
				i++;
			}
		}
		return i;
	}
	
	/**
	 * Determine si le joueur à cocher tous les scores
	 * @return
	 */
	public  boolean feuillePleine() {
		return nbrScoresRestants(this.f.getFeuille()) == 0;
	}
	
	/**
	 * Renvoie le score en fonction de la combi passé en paramêtre et de la clé voulu
	 * @param key indique quel score calculer
	 * @param combi Dés actuels
	 * @return Le score en fonction de la combinaison
	 */
	public static int renvoyerScore(String key,De[] combi) {
		int score = -1;
		switch(key) {
			case "Chance":
				score = De.scoreChance(combi);
				break;
			case "As":
				score = De.scoreHaut(combi,1);
				break;
			case "Deux":
				score = De.scoreHaut(combi,2);
				break;
			case "Trois":
				score = De.scoreHaut(combi,3);
				break;
			case "Quatre":
				score = De.scoreHaut(combi,4);
				break;
			case "Cinq":
				score = De.scoreHaut(combi,5);
				break;
			case "Six":
				score = De.scoreHaut(combi,6);
				break;
			case "Brelan":
				score = De.scoreBrelan(combi);
				break;
			case "Carré":
				score = De.scoreCarre(combi);
				break;
			case "Full":
				score = De.scoreFull(combi);
				break;
			case "Petite suite":
				score = De.scorePteSuite(combi);
				break;
			case "Grande suite":
				score = De.scoreGdeSuite(combi);
				break;
			case "Yathzee":
				score = De.scoreYathzee(combi);
				break;
		}
		return score;
	}
	
	/**
	 * Affiche un tableaux de String
	 * @param t tableau à afficher
	 */
	public static void afficherTab(String[] t) {
		for (int i = 0; i < t.length; i++) {
			System.out.print(t[i]+" | ");
		}
	}
	
	/**
	 * Détermine les combinaisons restantes
	 * @param f feuille de score du joueur
	 * @return le tableau des combi encore non choisit
	 */
	public static String[] getNonCoche(Map<String,Integer> f) {
		int restants = nbrScoresRestants(f);
		String[] cles = new String[restants];
		Set<String> keys = f.keySet();
		int i = 0;
		for (String key: keys) {
			if (f.get(key) == -1) {
				cles[i] = key;
				i++;
			}
		}
		return cles;
	}
	

	/**
	 * 
	 * @param key clé à chercher dans le tableau
	 * @param t tableau de recherche 
	 * @return -1 si pas dans le tableau l'indice sinon
	 */
	public static int inTable(String key, String[] t) {
		int in = -1;
		for (int i = 0; i < t.length; i++) {
			if (t[i] == key) {
				in = i;
			}
		}
		return in;
	}
	
	public static void bonusHaut(Joueur j){
		Map<String,Integer> f = j.getF().getFeuille();
		int scoreBonusHaut = f.get(Combinaison.As.getNom())+f.get(Combinaison.Deux.getNom())+f.get(Combinaison.Trois.getNom())+f.get(Combinaison.Quatre.getNom())+f.get(Combinaison.Cinq.getNom())+f.get(Combinaison.Six.getNom());
		System.out.println("Total Catégorie supérieure : "+scoreBonusHaut);
		if (scoreBonusHaut >= 63) {
			System.out.println("Bonus catégorie supérieur");
			j.score+= 35;
		}
	}

	
	/**
	 * Propose les scores restants  possible avec la combinaison et la feuille de score
	 * @param f
	 * @param combi Combinaison des dés
	 */
	public static void proposerScore(Joueur j, De[] combi) {
		Map<String,Integer> f = j.getF().getFeuille();
		String[] nonCoche = Joueur.getNonCoche(f);
		Set<String> keys = f.keySet();
		int x,choix = 0;
		Partie.afficherCombi(combi);
		System.out.println("Voici les scores possibles");
		for (String key: keys) {
			x = inTable(key, nonCoche);
			if (x != -1) {
				System.out.println(x+1+".  "+key+ " : "+renvoyerScore(key,combi));
			}else {
				System.out.println("(Déjà coché) "+key+" : "+f.get(key));
			}
		}
		while(choix < 1 || choix > nonCoche.length) {
			System.out.println("Il faut choisir un score : ");
			System.out.println("Merci de saisir un chiffre en 1 et "+nonCoche.length);
			choix = Partie.saisieEntier();
		}
		System.out.println("Score choisi :"+nonCoche[choix-1]);
		f.put(nonCoche[choix-1], renvoyerScore(nonCoche[choix-1],combi));
		j.score += f.get(nonCoche[choix-1]);
		j.afficherScore();
		
	}
	
}
