package yathzee.app.console;


public class Main {
	
	public static int choixNbrJoueurs() {
		int n = 0;
		System.out.println("Saisir le nombre de joueurs" );
		n = Partie.saisieEntier();
		return n;
	}
	
	
	public static void main(String[] args) {
		
		System.out.println("Bienvenue dans ce Yathzee");
		int n = choixNbrJoueurs();
		Partie p = new Partie(n, 1);
		p.jeu();

	
	}
}

