/**
 * @author Ruben Guenoun <guenoun.ruben@gmail.com>
 * @date 27/03/2020
 * Tous droits réservés
 */

package yathzee.app.console;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Partie {
	private int nbTours;
	private Joueur[] j;
	
	public Partie(int nbJoueurs, int nbTours) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		this.nbTours = nbTours;
		this.j = new Joueur[nbJoueurs];
		for (int i = 0; i <= nbJoueurs-1; i++) {
			System.out.println("Joueur "+(i+1)+", Veuillez saisir votre nom :");
			String nom = sc.nextLine();
			this.j[i] = new Joueur(nom);
			System.out.println(this.j[i]);
		}
	}
	
	public int getNbTours() {
		return this.nbTours;
	}
	
	
	/**
	 * Affiche une combinaison de dé
	 * @param combi Combinaison de dé à afficher
	 */
	public static void afficherCombi(De[] combi) {
		System.out.println("Voici la combinaison :");
 		for(int i= 0; i <= combi.length -1;i++) {
			System.out.print(combi[i].getValeur()+" | ");
		}
		System.out.println("");
	}
	
	/**
	 * Permet la saisie sécurisée d'un entier
	 * @return l'entier saisie par l'utilisateur
	 */
	@SuppressWarnings("resource")
	public static int saisieEntier() {
		int nbr = 0;
		boolean flag = false;
		Scanner sc;
		do {
			flag = false;
			sc = new Scanner(System.in);
			System.out.println("Saisir un nombre");
			try {
				if(sc.hasNextInt()) {
					nbr = sc.nextInt();
				}else {
					System.out.println("Erreur de  saisie");
					flag = true;
				}
			    } catch (InputMismatchException e) {
				System.out.println("Erreur de saisie");
				flag = true;
			    }
		}while(flag);
		return nbr;
	}
	
	
	/**
	 * Fonction qui séléctionne les dés à relancer
	 * @param combi
	 * @return Tableau avec les numéros des dés à relancer
	 */
	public static int[] selectionnerDes(De[] combi) {
		int [] numDes;
		int i = 0,nbrDes = 0;
		afficherCombi(combi);
		do {
			System.out.println("Combien de dés voulez vous relancer ? (Saisir un nombre entre 0 et 5)");
			nbrDes = saisieEntier();
		}while(nbrDes < 0 || nbrDes > 5);
		switch (nbrDes) {
		case 0: 
			System.out.println("Vous gardez donc tous vos dés.");
			numDes = new int[0];
			break;
		case 5:
			System.out.println("Vous relancez donc tout vos dés.");
			numDes = new int[5];
			break;
		default:
			System.out.println("Vous relancez donc "+nbrDes+" dés.");
			numDes = new int[nbrDes];
			while(i <= nbrDes-1) {
				System.out.println("Reste "+ (nbrDes-i) +" à choisir");
				afficherCombi(combi);
				System.out.println("Saisir le numéro du dé que vous voulez relancer");
				numDes[i] = saisieEntier();
				if (i > 0) {
					if(numDes[i-1] == numDes[i]){
						System.err.println("Vous avez déjà séléctionné ce dé");
						i--;
					}
				}
				if (numDes[i] < 1 || numDes[i] > 5) {
					System.err.println("Merci de sélectionner un dé");
				}else {
					i++;
				}
			}
		}
		return numDes;
	}
	
	/**
	 * Fonctions qui simule le lancer des dés
	 * @param nbrDes 
	 * @return la valeurs des dés
	 */
	public static  De[] lancerDes(int nbrDes) {
		De[] combi = new De[nbrDes];
		for (int i = 0;i <= nbrDes-1;i++) {
			combi[i] = new De();
			combi[i].lancer();
		}
		return combi;
	}
	
	/**
	 * Gère la relance de certains dés
	 * @param combi
	 * @param numDes
	 * @return la nouvelle combinaison de dés
	 */
	public static De[] relancerDes(De[] combi,int[] numDes) {
		// En fonction de la taille de numDes, on sait cb de dés relancer
		switch(numDes.length) {
			case 5:
				combi = lancerDes(5);
				break;
			case 0:
				System.out.println("Aucun dé à relancer");
				break;
			default:
				for(int i = 0; i <= numDes.length-1;i++) {
					combi[numDes[i]-1].lancer();
				}
				break;
		}
		return combi;
		
	}
	
	
	/**
	 * Simule le toure d'un joueur 
	 * @param j
	 */
	public static void tour(Joueur j) {
		int nbrLancers = 0;
		int[] numDes = new int[5];
		De[] combi = new De[5];
		System.out.println(j.getNom()+ ", c'est votre tour");
		while (nbrLancers <= 2) {/* || numDes.length > 0) {*/
			nbrLancers++;
			System.out.println("lancer n° "+nbrLancers);
			combi = relancerDes(combi, numDes);
			if(nbrLancers <3) {
				numDes = selectionnerDes(combi);
			}
			if(numDes.length == 0) {
				break;	
			}
			
			
		}
		Joueur.proposerScore(j, combi);
	}
	
	/**
	 * Lance le jeu
	 */
	public void jeu() {
		for (int k = 0; k < this.nbTours; k++) {
			while(!this.j[this.j.length-1].feuillePleine()) {
				for (int i = 0; i < this.j.length; i++) {
					tour(j[i]);
				}
			}
			System.out.println("Attribution des bonus : ");
			for (int i = 0; i < this.j.length; i++) {
				Joueur.bonusHaut(j[i]);
			}
		}
	}
	
	
}
