/**
 * @author Ruben Guenoun <guenoun.ruben@gmail.com>
 * @date 27/03/2020
 * Tous droits réservés
 * Gère les combinaisons possibles
 */

package yathzee.app.console;

public enum Combinaison {
	As("As","Nbre de 1 identiques","On additionne le nbr de dés qui ont 1 pour valeur"),
	Deux("Deux","Nbre de 2 identiques","On additionne le nbr de dés qui ont 2 pour valeur"),
	Trois("Trois","Nbre de 3 identiques","On additionne le nbr de dés qui ont 3 pour valeur"),
	Quatre("Quatre","Nbre de 4 identiques","On additionne le nbr de dés qui ont 4 pour valeur"),
	Cinq("Cinq","Nbre de 5 identiques","On additionne le nbr de dés qui ont 5 pour valeur"),
	Six("Six","Nbre de 6 identiques","On additionne le nbr de dés qui ont 6 pour valeur"),
	Brelan("Brelan","3 dés identiques", "On fait la somme des dés"),
	Carre("Carré","4 dés identiques","On fait la somme des dés"),
	Full("Full","1 paire et 1 brelan","25 pts"),
	PteSuite("Petite suite","4 dés qui se suivent (1,2,3,4) ou (2,3,4,5) ou (3,4,5,6)","30 pts"),
	GdeSuite("Grande suite","5 dés qui se suivent (1,2,3,4,5) ou (2,3,4,5,6)","40 pts"),
	Chance("Chance","Aucune conditions","On fait la somme des dés"),
	Yathzee("Yathzee","5 dés identiques", "50 pts");
	
	
	private String methode,nom,calcul;
	
	Combinaison(String nom, String methode, String calcul){
		this.nom = nom;
		this.methode = methode;
		this.calcul = calcul;
	}


	public String getMethode() {
		return methode;
	}

	public String getNom() {
		return nom;
	}

	public String getCalcul() {
		return calcul;
	}
	
	
	
}
