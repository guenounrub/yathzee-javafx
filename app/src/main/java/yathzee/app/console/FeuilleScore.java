package yathzee.app.console;

import java.util.Map;
import java.util.TreeMap;

public class FeuilleScore {
	private Map<String,Integer> feuille;

	public FeuilleScore() {
		this.feuille = new TreeMap<>();
		this.feuille.put(Combinaison.As.getNom(), -1);
		this.feuille.put(Combinaison.Deux.getNom(), -1);
		this.feuille.put(Combinaison.Trois.getNom(), -1);
		this.feuille.put(Combinaison.Quatre.getNom(), -1);
		this.feuille.put(Combinaison.Cinq.getNom(), -1);
		this.feuille.put(Combinaison.Six.getNom(), -1);
		this.feuille.put(Combinaison.Brelan.getNom(), -1);
		this.feuille.put(Combinaison.Carre.getNom(), -1);
		this.feuille.put(Combinaison.Full.getNom(), -1);
		this.feuille.put(Combinaison.PteSuite.getNom(), -1);
		this.feuille.put(Combinaison.GdeSuite.getNom(), -1);
		this.feuille.put(Combinaison.Yathzee.getNom(), -1);
		this.feuille.put(Combinaison.Chance.getNom(), -1);
	}

	public Map<String, Integer> getFeuille() {
		return feuille;
	}
	
	public void setScore(Combinaison c, int score) {
		this.feuille.put(c.getNom(), score);
	}
	
	public int getScore(Combinaison c) {
		return this.feuille.get(c.getNom());
	}
}
