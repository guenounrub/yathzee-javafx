module graphique {
	requires transitive javafx.graphics;
	requires transitive javafx.fxml;
	requires transitive javafx.controls;
	requires transitive javafx.base;
	
	opens graphique to javafx.fxml,javafx.graphics;
	exports graphique to javafx.graphics;
}